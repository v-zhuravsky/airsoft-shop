import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import Container from './components/Container';
import Row from './components/Row';

import Header from './components/Header';
import Sidebar from './components/Sidebar';
import ProductsList from './components/ProductsList';
import ProductsByCategory from './components/ProductsByCategory';
import Product from './components/Product';
import Search from './components/Search';
import Cart from './components/Cart';
import Checkout from './components/Checkout';
import Footer from './components/Footer';
import NotFound from './components/NotFound';

const App = () => {
	return (
		<div>
			<Header />
			<Container>
				<Row>
					<Sidebar />
					<Switch>
						<Route exact path="/" component={ProductsList} />
						<Route path="/search/:search" component={Search} />
						<Route path="/category/:category" component={ProductsByCategory} />
						<Route path="/product/:id" component={Product} />
						<Route path="/cart" component={Cart} />
						<Route path="/checkout" component={Checkout} />
						<Route component={NotFound} />
					</Switch>
				</Row>
			</Container>
			<Footer />
		</div>
	);
};

export default App;