import React from 'react';
import Icon from './Icon';

import '../styles/Footer.css';

const Footer = () => {
	return (
		<div className="footer">
			<div className="footer-top">
				<div className="block phone">
					<h2><Icon name="phone" />+38066666666</h2>
				</div>
				<div className="block mail">
					<h2><Icon name="envelope" />info@airsoft-amo.com</h2>
				</div>
			</div>
			<div className="footer-bottom">
				<p>Front-end by Viacheslav Zhuravskyi. Back-end by Vladimir Lantsov.</p>
				<h3>AirsoftAmo &copy; 2018.</h3>
			</div>
		</div>
	);
};

export default Footer;