import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { useInput } from '../hooks/useInput';

import ContainerFluid from './ContainerFluid';
import Row from './Row';
import Icon from './Icon';
import Img from './Img';

import '../styles/Header.css';

const Header = ({ cartSum, cartItems }) => {
	const search = useInput('');

	return (
		<div className="header">
			<ContainerFluid>
				<Row>
					<div className="logo col-md-3">
						<Link to="/"><h1>AirsoftAmo</h1></Link>
					</div>
					<div className="search col-md-4">
						<input name="search" {...search} type="text" placeholder="Search..." />
						<Link to={"/search/" + search.value.split(' ').map(part => part.toLowerCase()).join('_')}><Icon name="search" /></Link>
					</div>
					<div className="col-md-5">
						<div className="cart-show">
							<div className="cart-bar">
								<p><Icon name="shopping-cart" />Cart: {cartSum}$</p>
								<div className="dropdown">
									{
										cartItems.length > 0 ? cartItems.map(item => (
											<div key={item.product.productId} className="item">	
												<Img
													src={item.product.imgUrl}
													alt={item.product.productName}
													className="img-responsive"
												/>
												<div className="item-details">
													<Link
														to={"/product/" + item.product.productId}>
														{item.product.productName}
													</Link>
													<span className="amount">{item.amount}x</span>
													<span className="price">{item.amount * item.product.productPrice}$</span>
												</div>
											</div>
										)) : <h3>Your cart is empty</h3>
									}
									<Link className="go-cart" to="/cart"><Icon name="shopping-cart" />Go to cart</Link>
								</div>
							</div>
						</div>
					</div>
				</Row>
			</ContainerFluid>
		</div>
	);
};

const mapStateToProps = state => {
	let prices = [];
	if (state.cart.length > 0) {
		for (let i = 0; i < state.cart.length; i++) {
			prices.push(state.cart[i].amount * Number(state.cart[i].product.productPrice));
		}
	}

	return {
		cartSum: state.cart.length > 0 ? prices.reduce((prev, curr) => prev + curr) : 0,
		cartItems: state.cart
	};
};

export default connect(mapStateToProps)(Header);