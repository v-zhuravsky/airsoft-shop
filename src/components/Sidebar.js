import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import Icon from './Icon';

import '../styles/Sidebar.css';

const Sidebar = ({ cartSum }) => {
	return (
		<div className="col-md-3">
			<div className="sidebar">
				<div className="sidebar-header">
					<h2><Icon name="shopping-cart" />Cart: {cartSum}$</h2>
				</div>
			</div>
			<div className="sidebar">
				<div className="sidebar-header">
					<h2><Icon name="th-large" />Categories</h2>
				</div>
				<ul>
					<li><NavLink to="/category/rifles">Assault rifles</NavLink></li>
					<li><NavLink to="/category/machine_guns">Machine guns</NavLink></li>
					<li><NavLink to="/category/shotguns">Shotguns</NavLink></li>
					<li><NavLink to="/category/pistols">Pistols</NavLink></li>
					<li><NavLink to="/category/sniper">Sniper rifles</NavLink></li>
					<li><NavLink to="/category/gear">Tactical gear</NavLink></li>
					<li><NavLink to="/category/lanterns">Lanterns and light devices</NavLink></li>
				</ul>
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	let prices = [];
	if (state.cart.length > 0) {
		for (let i = 0; i < state.cart.length; i++) {
			prices.push(state.cart[i].amount * Number(state.cart[i].product.productPrice));
		}
	}

	return {
		cartSum: state.cart.length > 0 ? prices.reduce((prev, curr) => prev + curr) : 0
	};
};

export default connect(mapStateToProps)(Sidebar);