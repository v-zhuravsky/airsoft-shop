import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToCart } from '../actions/cartActions';

import Breadcrumbs from './Breadcrumbs';
import Row from './Row';
import Icon from './Icon';
import Img from './Img';
import Rating from './Rating';

import '../styles/Product.css';

class Product extends Component {
	state = {
		amountToOrder: 1
	}

	handlePlus = () => {
		this.setState({ amountToOrder: this.state.amountToOrder + 1 });
	}

	handleMinus = () => {
		const amount = this.state.amountToOrder;
		if (amount > 1) {
			this.setState({ amountToOrder: this.state.amountToOrder - 1 });
		}
	}

	render() {
		const { product, addToCart } = this.props;
		return (
			<div className="col-md-9 view-product">
				<Row>
					<div className="col-md-12">
						<Breadcrumbs location={this.props.location} />
					</div>
				</Row>
				<Row>
					<div className="col-md-6">
						<div className="product-img">
							<Img src={product.imgUrl} alt={product.productName} className="img-responsive" />
						</div>
						<div className="product-comments">
							{
								product.comments.length > 0
									? (
										<div className="comments-section">
											<h2>Comments: {product.comments.length}</h2>
											{
												product.comments.reverse().map(comment => (
													<div key={comment.commentId} className="comment">
														<h3>{comment.firstName}</h3>
														<p>{comment.commentText}</p>
														<p className="date">{comment.commentDate}</p>
													</div>
												))
											}
										</div>
									)
									: (
										<div className="comments-section">
											<h2>Comments</h2>
											<p>There are no comments for this product yet. Be the first.</p>
										</div>
									)
							}
						</div>
					</div>
					<div className="col-md-6">
						<div className="product-main">
							<h1>{product.productName} {product.productPrice}$</h1>
							<Rating stars={product.rating} />
							<div className="price-details">
								<span>
									<button className="minus" onClick={this.handleMinus}>-</button>
									<p>{this.state.amountToOrder}</p>
									<button className="plus" onClick={this.handlePlus}>+</button>
								</span>
								<button onClick={() => addToCart(this.state.amountToOrder, product)} className="add-to-cart"><Icon name="cart-plus" /></button>
							</div>
						</div>
						<div className="product-description">
							<p>{product.productDescription}</p>
						</div>
					</div>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		location: ownProps.location.pathname,
		product: state.products.filter(product => product.productId === Number(ownProps.match.params.id))[0],
	};
};

export default connect(mapStateToProps, { addToCart })(Product);