import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { useInput } from '../hooks/useInput';

import Row from './Row';
import Icon from './Icon';

import '../styles/Checkout.css';

const Checkout = ({ cart, cartSum }) => {
	const name = useInput('');
	const phoneNumber = useInput('');
	const location = useInput('');

	const handleSubmit = (e) => {
		e.preventDefault();
		alert(name.value + '\n' + phoneNumber.value + '\n' + location.value);
	};

	if (cart.length === 0) {
		return <Redirect to="/" />;
	}

	return (
		<div className="col-md-9">
			<Row>
				<div className="col-md-4">
					<form className="form" onSubmit={handleSubmit}>
						<h2>Delivery details</h2>
						<input
							type="text"
							placeholder="Name"
							{...name}
							name="name"
							required />
						<input
							type="text"
							placeholder="Phone number"
							{...phoneNumber}
							name="phoneNumber"
							required />
						<input
							type="text"
							placeholder="Delivery location"
							{...location}
							name="deliveryLocation"
							required />
						<button type="submit"><Icon name="shopping-cart" />Submit order</button>
					</form>
				</div>
				<div className="col-md-8">
					<div className="order-details">
						<h2>Products</h2>
						{
							cart.map(item => {
								return (
									<div key={item.product.productId} className="order-item">
										<p>{item.product.productName}</p>
										<div className="right">
											<p>{item.amount}x</p>
											<p>{item.amount * Number(item.product.productPrice)}$</p>
										</div>
									</div>
								);
							})
						}
						<div className="ovh">
							<h3>Total price: {cartSum}$</h3>
						</div>
					</div>
				</div>
			</Row>
		</div>
	);
};

const mapStateToProps = state => {
	let prices = [];
	if (state.cart.length > 0) {
		for (let i = 0; i < state.cart.length; i++) {
			prices.push(state.cart[i].amount * Number(state.cart[i].product.productPrice));
		}
	}

	return {
		cart: state.cart,
		cartSum: state.cart.length > 0 ? prices.reduce((prev, curr) => prev + curr) : 0
	};
};

export default connect(mapStateToProps)(Checkout);