import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { changeProductAmount, deleteFromCart } from '../actions/cartActions';

import Row from './Row';
import Img from './Img';
import Icon from './Icon';
import Rating from './Rating';

import '../styles/Cart.css';

const Cart = ({ cart, cartSum, deleteFromCart, changeProductAmount }) => {
	return (
		<div className="col-md-9">
			<div className="cart">
				{
					cart.length > 0
						? <h2 style={{ margin: '30px 30px 0' }}>Total price: {cartSum}$</h2>
						: <h2 style={{ margin: '30px' }}>Your cart is empty</h2>
				}
				{
					cart.map(item => (
						<div key={item.product.productId} className="cart-item">
							<Row>
								<div className="col-md-3">
									<div className="img">
										<Img
											src={item.product.imgUrl}
											alt={item.product.productName}
											className="img-responsive" />
									</div>
								</div>
								<div className="col-md-7">
									<div className="details">
										<Link to={"/product/" + item.product.productId}>
											{item.product.productName}
										</Link>
										<Rating stars={item.product.rating} />
										<p>Amount: {item.amount}</p>
										<p>Single price: {item.product.productPrice}$</p>
										<p>Total price: {item.amount * Number(item.product.productPrice)}$</p>
									</div>
									<div className="controls">
										<button
											onClick={() => changeProductAmount(item.product.productId, item.amount - 1)}
											className="minus"
										>
											<Icon name="minus-square" />
										</button>
										<span>{item.amount}</span>
										<button
											onClick={() => changeProductAmount(item.product.productId, item.amount + 1)}
											className="plus"
										>
											<Icon name="plus-square" />
										</button>
									</div>
								</div>
								<div className="col-md-2">
									<div className="delete">
										<button onClick={() => deleteFromCart(item.product.productId)}><Icon name="trash-o" />Delete</button>
									</div>
								</div>
							</Row>
						</div>
					))
				}
				{
					cart.length > 0 && <Link to="/checkout" className="checkout"><Icon name="shopping-cart" />Submit order</Link>
				}
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	let prices = [];
	if (state.cart.length > 0) {
		for (let i = 0; i < state.cart.length; i++) {
			prices.push(state.cart[i].amount * Number(state.cart[i].product.productPrice));
		}
	}

	return {
		cart: state.cart,
		cartSum: state.cart.length > 0 ? prices.reduce((prev, curr) => prev + curr) : 0
	};
};

export default connect(mapStateToProps, { changeProductAmount, deleteFromCart })(Cart);