import React from 'react';
import { connect } from 'react-redux';

import Products from './Products';

const Search = ({ products, location }) => {
	if (products.length < 1) {
		return <h1>Sorry, but we can't find product you're looking for. Probably we don't sell it yet. If you want to order it, then you can contact us and we will order this product directly for you.</h1>
	}
	return <Products location={location} products={products} />;
};

const mapStateToProps = (state, ownProps) => {
	let searchParts = ownProps.match.params.search.split('_').map(part => part.toLowerCase());

	return {
		location: ownProps.location.pathname,
		products: state.products.filter(product => {
			let nameParts = product.productName.split(' ').map(part => part.toLowerCase());

			for (let i = 0; i < searchParts.length; i++) {
				for (let j = 0; j < nameParts.length; j++) {
					if (nameParts[j] === searchParts[i]) {
						return product;
					}
				}
			}
		})
	};
};

export default connect(mapStateToProps)(Search);