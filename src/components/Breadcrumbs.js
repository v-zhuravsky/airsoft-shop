import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Icon from './Icon';
import '../styles/Breadcrumbs.css';

const Breadcrumbs = ({ location, products }) => {
	const locationParts = location.split('/');

	if (locationParts.length > 0) {
		if (locationParts[1] === "category" && locationParts[2] === "rifles") {
			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to="/category/rifles">Assault rifles</Link>
			</div>);
		} else if (locationParts[1] === "category" && locationParts[2] === "machine_guns") {
			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to="/category/machine_guns">Machine guns</Link>
			</div>);
		} else if (locationParts[1] === "category" && locationParts[2] === "shotguns") {
			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to="/category/shotguns">Shotguns</Link>
			</div>);
		} else if (locationParts[1] === "category" && locationParts[2] === "pistols") {
			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to="/category/pistols">Pistols</Link>
			</div>);
		} else if (locationParts[1] === "category" && locationParts[2] === "sniper") {
			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to="/category/sniper">Sniper rifles</Link>
			</div>);
		} else if (locationParts[1] === "category" && locationParts[2] === "gear") {
			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to="/category/gear">Tactical gear</Link>
			</div>);
		} else if (locationParts[1] === "category" && locationParts[2] === "lanterns") {
			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to="/category/lanterns">Lanterns and light devices</Link>
			</div>);
		} else if (locationParts[1] === "product") {
			let product = products.filter(product => product.productId === Number(locationParts[2]))[0];
			let category = product.category;
			let categoryToInsert = "";
			let linkToInsert = "/category/" + category;

			if (category === "rifles") {
				categoryToInsert = "Assault rifles";
			} else if (category === "machine_guns") {
				categoryToInsert = "Machine guns";
			} else if (category === "shotguns") {
				categoryToInsert = "Shotguns";
			} else if (category === "pistols") {
				categoryToInsert = "Pistols";
			} else if (category === "sniper") {
				categoryToInsert = "Sniper rifles";
			} else if (category === "gear") {
				categoryToInsert = "Tactical gear";
			} else if (category === "lanterns") {
				categoryToInsert = "Lanterns and light devices";
			}

			return (<div className="breadcrumbs">
				<Link to="/">Main page</Link>
				<Icon name="long-arrow-right" />
				<Link to={linkToInsert}>{categoryToInsert}</Link>
				<Icon name="long-arrow-right" />
				<Link to={"/product/" + product.productId}>{product.productName}</Link>
			</div>);
		} else {
			return null;
		}
	} else {
		return null;
	}
};

const mapStateToProps = (state, ownProps) => {
	return {
		location: ownProps.location,
		products: state.products
	};
};

export default connect(mapStateToProps)(Breadcrumbs);