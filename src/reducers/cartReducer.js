import {
	ADD_TO_CART,
	DELETE_FROM_CART,
	CHANGE_PRODUCT_AMOUNT
} from '../actions/types';

const initialState = [];

const cartReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_TO_CART:
			for (let i = 0; i < state.length; i++) {
				if (state[i].product.productId === action.payload.product.productId) {
					return state.map(el => {
						if (el.product.productId === action.payload.product.productId) {
							return { amount: el.amount + action.payload.amount, product: el.product }
						}
						return el;
					});
				}
			}
			return [
				...state,
				action.payload
			];
		case CHANGE_PRODUCT_AMOUNT:
			return state.map(item => {
				if (item.product.productId === action.payload.id && action.payload.newAmount > 0) {
					return { amount: action.payload.newAmount, product: item.product };
				} else {
					return item;
				}
			});
		case DELETE_FROM_CART:
			return state.filter(item => item.product.productId !== action.payload);
		default:
			return state;
	}
};

export default cartReducer;