import { combineReducers } from 'redux';

import productsReducer from './productsReducer';
import flashMessagesReducer from './flashMessagesReducer';
import cartReducer from './cartReducer';

const rootReducer = combineReducers({
	products: productsReducer,
	flashMessages: flashMessagesReducer,
	cart: cartReducer
});

export default rootReducer;