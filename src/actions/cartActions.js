import {
	ADD_TO_CART,
	DELETE_FROM_CART,
	CHANGE_PRODUCT_AMOUNT
} from './types';

export const addToCart = (amount, product) => dispatch => {
	dispatch({
		type: ADD_TO_CART,
		payload: { amount, product }
	});
};

export const changeProductAmount = (id, newAmount) => dispatch => {
	dispatch({
		type: CHANGE_PRODUCT_AMOUNT,
		payload: { id, newAmount }
	});
};

export const deleteFromCart = id => dispatch => {
	dispatch({
		type: DELETE_FROM_CART,
		payload: id
	});
};